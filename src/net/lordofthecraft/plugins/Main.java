package net.lordofthecraft.plugins;

/**
 * Heads and Flowers
 * 
 * @author Arzota
 * @author Katherine1
 * 
 * Adds corpses to loot and an easier time collecting flowers
 */

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Skull;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.MaterialData;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

//This is a test
public class Main extends JavaPlugin implements Listener {
	private final Logger logger = Logger.getLogger("Minecraft");
	private final Plugin plugin = this;
	private HashMap<Location, Inventory> inv;
	

	@Override
	public void onEnable() {
		//Start up the logger
		PluginDescriptionFile pdfFile = this.getDescription();
		this.logger.info(pdfFile.getName() + pdfFile.getVersion()
				+ "Has Been Enabled.");
		//Register events
		getServer().getPluginManager().registerEvents(
				this, this);
		//Initialize HashMap
		inv = new HashMap<Location,Inventory>();

	}
	

	@Override
	public void onDisable() {
		int test = null;
		PluginDescriptionFile pdfFile = this.getDescription();
		this.logger.info(pdfFile.getName() + "Has Been Disabled.");
		
		Iterator<Location> i = inv.keySet().iterator();
		while (i.hasNext()) {
			removeSkull(i.next());
		}
	}
	
	/**
	 * removeSkull(location)
	 * Tries to remove a skull at a given location
	 * @param loc - The location of the skull
	 */
    public void removeSkull(Location loc) {
    	//Try getting an inventory with the location
        Inventory inventory = inv.get(loc);
        //If there is an inventory, there's a skull
        if ( inventory != null ) {
        	inv.remove(loc);
        	//use location to change block at location to air if it is a skull
        	Block skull = loc.getBlock();
        	if (skull.getType() == Material.SKULL) {
        		skull.setType(Material.AIR);
        	}
        }
    }
    
    /**
     * place(PlayerDeathEvent)
     * Place a skull with inventory on a PlayerDeathEvent
     * @param event - The event
     */
    @EventHandler(priority = EventPriority.NORMAL)
    public void place(PlayerDeathEvent event){
    	//Get the player and their name
    	Player player = (Player) event.getEntity();
    	String name = player.getDisplayName();
    	//Make inv name for below
    	String InitialN = ChatColor.WHITE + name + ChatColor.YELLOW + "'s corpse";
    	String AltN = ChatColor.WHITE + "A nameless" + ChatColor.YELLOW + " corpse";
    	//Check inv name is below 32 characters
    	Integer namelen = InitialN.length();
    	//Make the inventory
    	Inventory inventory = (namelen >= 32 ? Bukkit.createInventory(null,45,(AltN)) : Bukkit.createInventory(null,45,(InitialN)));
    	//Get the list of items dropped by the player
    	List<ItemStack> items = event.getDrops();
    	//Add the items to the inventory
    	Iterator<ItemStack> i = items.iterator();
    	while (i.hasNext()) {
    	inventory.addItem(i.next());
    	}   	
    	
    	//Make the skull --
        Block b = player.getLocation().getBlock();
        //While the skull is in the air, drop it down until something is found.
        while (b.getType() == Material.AIR) {
         Location loc = b.getLocation();
         loc.setY(loc.getY()-1f);
         b = loc.getBlock();
        }
        //While the skull is underground/water
        while (b.getType() != Material.AIR) {
         Location loc = b.getLocation();
         loc.setY(loc.getY()+1f);
         b = loc.getBlock();
        }
        b.setType(Material.SKULL);
        
        //Clear drops
        event.getDrops().clear();
  
    	Location skullLocation = b.getLocation();
    	//Set skull rotation here.
    	//Note: Doesn't work because things are screwed with Minecraft
    	Skull s = (Skull) b.getState();
    	float rot = player.getLocation().getYaw();
    	
    	if (rot > 11.25 && rot <= 33.75 ) {
    		s.setRotation(BlockFace.SOUTH_SOUTH_WEST);
    		s.update();
    	}
    	else if (rot > 33.75 && rot <= 56.25) {
    		s.setRotation(BlockFace.SOUTH_WEST);
    		s.update();
    	}
    	else if (rot > 56.25 && rot <= 78.75) {
    		s.setRotation(BlockFace.WEST_SOUTH_WEST);
    		s.update();
    	}
    	else if (rot > 78.75 && rot <= 101.25) {
    		s.setRotation(BlockFace.WEST);
    		s.update();
    	}
    	else if (rot > 101.25 && rot <= 123.75) {
    		s.setRotation(BlockFace.WEST_NORTH_WEST);
    		s.update();
    	}
    	else if (rot > 123.75 && rot <= 146.25) {
    		s.setRotation(BlockFace.NORTH_WEST);
    		s.update();
    	}
    	else if (rot > 146.25 && rot <= 168.75) {
    		s.setRotation(BlockFace.NORTH_NORTH_WEST);
    		s.update();
    	}
    	else if (rot > 168.75 && rot <= 191.25) {
    		s.setRotation(BlockFace.NORTH);
    		s.update();
    	}
    	else if (rot > 191.25 && rot <= 213.75) {
    		s.setRotation(BlockFace.NORTH_NORTH_EAST);
    		s.update();
    	}
    	else if (rot > 213.75 && rot <= 236.25) {
    		s.setRotation(BlockFace.NORTH_EAST);
    		s.update();
    	}
    	else if (rot > 236.25 && rot <= 258.75) {
    		s.setRotation(BlockFace.EAST_NORTH_EAST);
    		s.update();
    	}
    	else if (rot > 258.75 && rot <= 281.25) {
    		s.setRotation(BlockFace.EAST);
    		s.update();
    	}
    	else if (rot > 281.25 && rot <= 303.75) {
    		s.setRotation(BlockFace.EAST_SOUTH_EAST);
    		s.update();
    	}
    	else if (rot > 303.75 && rot <= 326.25) {
    		s.setRotation(BlockFace.SOUTH_EAST);
    		s.update();
    	}
    	else if (rot > 326.25 && rot <= 348.75) {
    		s.setRotation(BlockFace.SOUTH_SOUTH_EAST);
    		s.update();
    	}
    	else {
    		s.setRotation(BlockFace.SOUTH);
    		s.update();
    	}
    	
    	//Set to the player's head
    	s.setSkullType(SkullType.PLAYER);
    	s.setOwner(player.getName());
    	s.update();
    	
    	//Store location with inventory
    	inv.put(skullLocation, inventory); 
    	//Set to remove the skull later
    	SkullCleaner clean = new SkullCleaner(skullLocation);
    	clean.runTaskLater(plugin, 6000);

    }
    
    /**
     * onBlockBreak(BlockBreakEvent)
     * Make sure a skull is dropped instead of a player head
     * @param event - The event
     */
    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
    	//If a skull block with an inventory.
    	if (event.getBlock().getType() == Material.SKULL 
    			&& inv.get(event.getBlock().getLocation()) != null) {
    		//cancel normal break event
    		event.setCancelled(true);
    		//drop a named skull
    		ItemStack skull = new ItemStack(Material.SKULL_ITEM);
    		ItemMeta meta = skull.getItemMeta();
    		meta.setDisplayName(inv.get(event.getBlock().getLocation()).getName());
    		skull.setItemMeta(meta);
    		event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(), skull);
    		//Remove the head on the ground.
    		removeSkull(event.getBlock().getLocation());
    	}
    }
    
    /**
     * onPlayerInteract(PlayerInteractEvent)
     * Run the two interact methods
     * @param event - The event
     */
    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
    	skullOpen(event);
    	flowersGrow(event);
    }
    
    /**
     * skullOpen(PlayerInteractEvent)
     * Open the inventory of a player corpse
     * @param event - The event
     */
    private void skullOpen(PlayerInteractEvent event) {
    	//Get the player and the inventory
    	Player player = event.getPlayer();
    	Inventory inventory = event.hasBlock() ? inv.get(event.getClickedBlock().getLocation()) : null; //Grab an inventory from the map
    	//If the head is getting right clicked and the inventory isn't null
    	if (inventory != null && event.getAction() == Action.RIGHT_CLICK_BLOCK) { //If it's not null and was a right click, check it.
    		//Start opening the inventory
    		player.sendMessage(ChatColor.YELLOW + "You begin searching " + inventory.getName() + ". Don't move!");
    		SkullOpener open = new SkullOpener(inventory, event.getPlayer(), player.getLocation());
    		//5 second delay
    		open.runTaskLater(this, 100);
    	}
    }
	
    /**
     * flowersGrow(PlayerInteractEvent)
     * Mimics the behavior of bonemeal on double tall flowers with
     * normal height flowers and a couple additional plants.
     * @param event - The event
     */
	@SuppressWarnings( "deprecation" )
	public void flowersGrow(PlayerInteractEvent event) {
		//If using bonemeal on something.
		if (event.getAction() == Action.RIGHT_CLICK_BLOCK
				&& event.hasBlock()
				&& event.getPlayer().getItemInHand().getData().equals(new MaterialData(351,(byte)15))
				&& event.getPlayer().getItemInHand().getAmount() > 0) {
			
			Block block = event.getClickedBlock();
			
			//Still no alternative to data values, despite deprecation.
			switch(block.getTypeId()) { //Using ID's rather than material enum because of below note
			//Drop single tall plants that aren't ferns or grass. Because ferns and grass grow into tall ferns and grass.
			case 31: //Grass/bush/fern
				if (block.getData() == 2 || block.getData() == 1) {
					break;
				}
			case 32: //Dead bush
			case 37: //Dandelion
			case 38: //Single Flowers
			case 111: //Lily Pads
				block.getWorld().dropItemNaturally(block.getLocation(), new ItemStack(block.getType(), 1, block.getData()));
				if (event.getPlayer().getItemInHand().getAmount() == 1) {
					event.getPlayer().setItemInHand(null);
				}
				else {
					event.getPlayer().getItemInHand().setAmount(event.getPlayer().getItemInHand().getAmount()-1);
				}
				break;
			case 175: //Not in the Material enum yet. Seriously. Double tall plants.
				//If the bottom half of a giant fern, drop a fern.
				if ((block.getData() & 0x7) == 3) {
					block.getWorld().dropItemNaturally(block.getLocation(), new ItemStack(Material.LONG_GRASS, 1, (byte)2));
					if (event.getPlayer().getItemInHand().getAmount() == 1) {
						event.getPlayer().setItemInHand(null);
					}
					else {
						event.getPlayer().getItemInHand().setAmount(event.getPlayer().getItemInHand().getAmount()-1);
					}
				}
				//Else if the top half of a double tall thing, find out if it's a fern.
				else if (block.getData() >= 8) {
					Location loc = block.getLocation().clone();
					loc.setY(loc.getBlockY()-1);
					Block b2 = block.getWorld().getBlockAt(loc);
					//If it is, drop a fern
					if ((b2.getData() & 0x7) == 3) {
						block.getWorld().dropItemNaturally(block.getLocation(), new ItemStack(Material.LONG_GRASS, 1, (byte)2));
						if (event.getPlayer().getItemInHand().getAmount() == 1) {
							event.getPlayer().setItemInHand(null);
						}
						else {
							event.getPlayer().getItemInHand().setAmount(event.getPlayer().getItemInHand().getAmount()-1);
						}//else
					}//if - dropping a fern
				}//else if - top half
				break;
			}//switch
		}//if - using bonemeal
	}//flowersGrow
	
	/**
	 * SkullCleaner
	 * Cleans up a given skull when run.
	 */
	private class SkullCleaner extends BukkitRunnable {

    	Location skullLoc;
        
    	public SkullCleaner(Location loc) {
    		skullLoc = loc.clone();
    	}

    	@Override
    	public void run() {
    		removeSkull(skullLoc);
    	}
    }
	
	/**
	 * SkullOpener
	 * Opens up a given skull for a given player when run.
	 */
	private class SkullOpener extends BukkitRunnable {
		Inventory inventory;
		Player player;
		Location location;
		
		public SkullOpener(Inventory i, Player p, Location l) {
			inventory = i;
			player = p;
			location = l.clone();
		}
		
		public void run() {
			if (player.isOnline() 
					&& player.getLocation().getBlockX() == location.getBlockX()
					&& player.getLocation().getBlockY() == location.getBlockY()
					&& player.getLocation().getBlockZ() == location.getBlockZ()) {
    			player.openInventory(inventory); //Open the inventory!
			}
		}
	}
}